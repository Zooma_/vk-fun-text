// ==UserScript==
// @name         vk fun text
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  mAkE FuN CoNvErSaTiOn gReAt aGaIn!
// @match        https://vk.com/im?sel=*
// @match        https://vk.com/im?peers=*
// @UpdateURL    https://gitlab.com/Zooma_/vk-fun-text/-/blob/master/main.js
// @author       Zooma  telegram: @de_zooma
// @grant        none
// ==/UserScript==

function random_case(str) {
  let random_case_str = "";
  for (let char of str) {
    random_case_str += (Math.random() > 1 / 2) ? char.toUpperCase() : char.toLowerCase();
  }
  return random_case_str;
}

function lower_upper_case(str) {
  let low_upper_case_str = "";
  for (let i = 0; i < str.length; i++) {
    low_upper_case_str += (i % 2) ? str[i].toUpperCase() : str[i].toLowerCase();
  }
  return low_upper_case_str;
}

(async function() {
  let toggle_click = 1;
  let vk_input = document.querySelector('[id^=im_editable]');

  let funtext_input = document.createElement("input");
  funtext_input.type = "text";
  funtext_input.id = "funtext_input";
  funtext_input.setAttribute("style", "margin-bottom: 10px;width: 90%;");

  let checkbox = document.createElement('input');
  checkbox.type = "checkbox";
  checkbox.id = "toggle_random";
  checkbox.setAttribute("checked", toggle_click);
  checkbox.value = toggle_click;

  let text_container = document.getElementsByClassName('im-chat-input clear_fix im-chat-input_classic _im_chat_input_parent')[0];
  text_container.appendChild(funtext_input);
  text_container.appendChild(checkbox);


  checkbox.addEventListener('click', function(event) {
    if(checkbox.value == 0) {
      checkbox.value = 1;
      //console.log(checkbox.value);
    }
    else if (checkbox.value == 1) {
      checkbox.value = 0;
      //console.log(checkbox.value);
    }
  });

  funtext_input.addEventListener('input', function (event) {
    if(checkbox.value == 1) {
      //console.log('lower_upper_case');
      vk_input.innerHTML = lower_upper_case(funtext_input.value);
    }
    else if(checkbox.value == 0) {
      //console.log('random_case');
      vk_input.innerHTML = random_case(funtext_input.value);
    }
    document.getElementsByClassName('ph_content')[0].textContent = '';
  });


  funtext_input.addEventListener('keyup', function (event) { // catch enter in my input for submit text through vk input
    if(event.code == 13 || event.which == 13) {
      document.getElementsByClassName('im-send-btn im-chat-input--send _im_send im-send-btn_audio')[0].className = 'im-send-btn im-chat-input--send _im_send im-send-btn_send';
      document.getElementsByClassName('im-send-btn im-chat-input--send _im_send im-send-btn_send')[0].click();
      funtext_input.value = '';
    }
  });

})();
